FROM alpine:3.12

WORKDIR /sexbot
COPY sex.c /sexbot
COPY app /sexbot/app

RUN apk -U update && apk --update-cache add build-base bash
RUN gcc -o /sexbot/sex /sexbot/sex.c

ENTRYPOINT ["/sexbot/sex"]
