from os import path, environ
from sys import exit

import click
from mastodon import Mastodon


@click.command()
@click.option(
    '--url',
    prompt=True,
    default=lambda: environ.get('MASTODON_URL', ''),
    help='Mastodon base URL'
)
@click.option(
    '--name',
    prompt=True,
    default=lambda: environ.get('MASTODON_NAME', ''),
    help='Mastodon app name'
)
@click.option(
    '--login',
    prompt=True,
    default=lambda: environ.get('MASTODON_LOGIN', ''),
    help='Mastodon login username'
)
@click.option(
    '--password',
    prompt=True,
    default=lambda: environ.get('MASTODON_PASSWORD', ''),
    help='Mastodon password'
)
@click.option(
    '--verbose',
    default=False,
    is_flag=True,
    help='Enable verbose Python exceptions'
)
def register_app(url, name, login, password, verbose):
    client_creds = '{name}-client_creds.secret'.format(
        name=name
    )

    if not path.isfile(client_creds):
        try:
            Mastodon.create_app(
                name,
                api_base_url=url,
                to_file='{name}-client_creds.secret'.format(
                    name=name
                )
            )
        except Exception as e:
            if verbose: raise
            print('Creating app failed: {error}'.format(
                error=str(e)
            ))
            exit(-1)

    user_creds = '{name}-user_creds.secret'.format(
        name=name
    )

    if not path.isfile(user_creds):
        try:
            mastodon = Mastodon(
                client_id=client_creds,
                api_base_url=url
            )
            mastodon.log_in(
                login,
                password,
                to_file=user_creds
            )
        except Exception as e:
            if verbose: raise
            print('Login failed: {error}'.format(
                error=str(e)
            ))
            exit(-1)


if __name__ == '__main__':
    exit(register_app(None, None))

