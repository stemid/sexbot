import sys
from os import environ

import click
from mastodon import Mastodon


@click.command()
@click.option(
    '--url',
    metavar='MASTODON_URL',
    envvar='MASTODON_URL',
    help='Mastodon base URL'
)
def post(url):
    sexquote = sys.stdin.readline()

    mastodon = Mastodon(
        access_token='{app_name}-user_creds.secret'.format(
            app_name='sexquote'
        ),
        api_base_url=url
    )

    mastodon.status_post(
        sexquote,
        sensitive=True,
        spoiler_text='Daily sexquote',
        language='en'
    )


if __name__ == '__main__':
    sys.exit(post(None, None))

